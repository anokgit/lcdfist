/**
 * @file
 * Global utilities.
 *
 */
(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.bootstrap_barrio_subtheme = {
    attach: function (context, settings) {
        initSlick();

    }
  };
   
   
  $(document).ready(function($) {
      
    $(".block-views-blockprocurement-block-1 .view-content span:not(:first)").each(function() {
        
        var isActive = $(this).find("a");
        
        if( !isActive ) {
            $(".block-views-blockprocurement-block-1 .view-content span:first-of-type a").addClass("is-active");
        }
        
    });
    
    $(".block-views-blockprocurement-block-1 .view-content span a").click(function() {
        $(".block-views-blockprocurement-block-1 .view-content span:first-of-type a").removeClass("is-active");
        $(this).addClass("is-active");
        
    });
      

    if ($(window).width() >= 992 ) {  
      $('li.dropdown a').on('click', function(){
                
            if($(this).attr('href')){
                window.location = $(this).attr('href');
            }
            
        });
     }
     
          
    $( "#edit-name" ).keypress(function(e) {
                var key = e.keyCode;
                if (key >= 48 && key <= 57) {
                    e.preventDefault();
                }
            });
         
      
    $(".page-node-48 h1.title span").html("Contact Us");
    
    $("p.slide-body").text(function(index, currentText) {
       return currentText.substr(0, 200);
    });

    $('.slick-slide').each(function() {
        var t = $(this).find('.slide-tit').text();
        var b = $(this).find('.slide-body').text();
        
        if(t.length < 1 && b.length < 1) {
            $(this).find('.slide-txt').css('display','none');
        }
        
    });  
    
    $('#search-block-form input#edit-keys').attr('placeholder','SEARCH');
    $('#search-block-form input#edit-keys--2').attr('placeholder','SEARCH');
  
      
    $(".block-search-form-block #edit-keys").click(function() {
        $("div#block-searchform p").css('display', 'none');
    });

    $( ".block-search-form-block #edit-keys" ).focusout(function() {
        if( $('.block-search-form-block #edit-keys').val().length === 0 ) {
            $("div#block-searchform p").css('display', 'block');
         }
    });

      
    $("#loader").fadeOut( "slow", function() {
        $("#loader").hide();
    });

    $("#search-block-form .form-actions .btn-primary").text("");

    $('div#block-searchform').find('h2').replaceWith(function() {
        return '<p>' + $(this).text() + '</p>';
    });

    $('.path-frontpage #block-bootstrap-barrio-subtheme-page-title').find('h1').replaceWith(function() {
        return '<h2>' + $(this).text() + '</h2>';

    });

});


    $(window).on('load', function(){
        
    
    $(".primary-bg").css("opacity", "1")
    $("#myDev").css("display", "block")
    $("img").removeAttr("title");
    $("a.navbar-brand").removeAttr("title")
    $("a.colorbox.cboxElement").removeAttr("title")
    $("img.cboxPhoto").removeAttr("title")
    $(".field__items img").removeAttr("title")
    $("div.bor-bot a").attr("rel","nofollow");
    $(".node__content  a").attr("target","_blank")

    });
  




    function initSlick() {
    $('.slide-item ul').not('.slick-initialized').slick({
         dots:           false,
         infinite:       true,
         speed:          300,
         slidesToShow:   1,
         autoplay:       true,
         draggable:      false,
         adaptiveHeight: true,
         arrows:         false
    });
    }
    
    
    var position;
    $(document).bind('cbox_open', function() {
        position = $("html,body").scrollTop();
        $('html').css({
            overflow: 'hidden'
        });
    }).bind('cbox_closed', function() {
        $('html').css({
            overflow: 'auto'
        });
        $("html,body").scrollTop(position);
    });



})(jQuery, Drupal);
