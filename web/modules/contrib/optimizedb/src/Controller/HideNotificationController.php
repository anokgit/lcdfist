<?php

namespace Drupal\optimizedb\Controller;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Page hide notification.
 */
class HideNotificationController extends ControllerBase {

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Controller constructor.
   *
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   The date formatter.
   */
  public function __construct(TimeInterface $time, ConfigFactoryInterface $configFactory, DateFormatterInterface $dateFormatter) {
    $this->time = $time;
    $this->configFactory = $configFactory;
    $this->dateFormatter = $dateFormatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('datetime.time'),
      $container->get('config.factory'),
      $container->get('date.formatter')
    );
  }

  /**
   * Page hide notification.
   *
   * @return string
   *   Result hide notification.
   */
  public function hide() {
    $time = $this->time->getRequestTime();
    $config = $this->configFactory->getEditable('optimizedb.settings');

    $notify_optimize = $config->get('notify_optimize');

    // There is a need to disable the notification?
    if ($notify_optimize) {
      $config
        ->set('notify_optimize', FALSE)
        // If the notification of the need to optimize hiding, so it runs.
        ->set('last_optimization', $time)
        ->save();

      $optimization_period = (int) $config->get('optimization_period');
      $time_next_optimization = strtotime('+ ' . $optimization_period . ' day', $time);

      $output = $this->t('The following message on the need to perform optimization, you get - @date.', [
        '@date' => $this->dateFormatter->format($time_next_optimization),
      ]);
    }
    else {
      $output = $this->t('Alerts are not available.');
    }

    return [
      '#type' => 'markup',
      '#markup' => $output,
    ];
  }

}
