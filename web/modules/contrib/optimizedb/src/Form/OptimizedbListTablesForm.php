<?php

namespace Drupal\optimizedb\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Utility\TableSort;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Operations with tables.
 */
class OptimizedbListTablesForm extends FormBase {

  /**
   * The request stack used for testing.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The database connection used.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a new FileTransferAuthorizeForm object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   * @param \Drupal\Core\Database\Connection $database
   *   The database service.
   */
  public function __construct(RequestStack $requestStack, Connection $database) {
    $this->requestStack = $requestStack;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'optimizedb_list_tables_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $headers = [
      'name' => [
        'data' => $this->t('Table name'),
      ],
      'size' => [
        'data' => $this->t('Table size'),
        'field' => 'size',
        'sort' => 'desc',
      ],
    ];

    $tables = _optimizedb_tables_list();

    $sort = TableSort::getSort($headers, $this->requestStack->getCurrentRequest());

    usort($tables, function ($a, $b) use ($sort) {
      if ($sort == 'asc') {
        return $a['size_byte'] > $b['size_byte'];
      }

      return $a['size_byte'] < $b['size_byte'];
    });

    $rows = [];

    // Messages status execute operation.
    optimizedb_operation_messages($form);

    foreach ($tables as $table) {
      // Parameter "size_byte" is only needed to sort, now this unit to remove.
      unset($table['size_byte']);

      $rows[$table['name']] = $table;
    }

    if ($this->database->driver() == 'mysql') {
      $form['operations'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Operations with tables:'),
      ];

      $form['operations']['check_tables'] = [
        '#type' => 'submit',
        '#value' => $this->t('Check tables'),
      ];

      $form['operations']['repair_tables'] = [
        '#type' => 'submit',
        '#value' => $this->t('Repair tables'),
      ];

      $form['operations']['optimize_tables'] = [
        '#type' => 'submit',
        '#value' => $this->t('Optimize tables'),
      ];
    }

    $form['tables'] = [
      '#type' => 'tableselect',
      '#header' => $headers,
      '#options' => $rows,
      '#empty' => $this->t('No content available.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $tables = $form_state->getValue('tables');
    $operation = '';

    $op = (string) $form_state->getValue('op');

    switch ($op) {
      // Checking the selected tables to find errors.
      case $this->t('Check tables'):
        $operation = 'CHECK TABLE';
        break;

      // Repair selected tables.
      case $this->t('Repair tables'):
        $operation = 'REPAIR TABLE';
        break;

      // Optimization of the selected tables.
      case $this->t('Optimize tables'):
        $operation = 'OPTIMIZE TABLE';
        break;
    }

    _optimizedb_list_tables_operation_execute($tables, $operation);
  }

}
